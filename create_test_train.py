# -*- coding: utf-8 -*-
"""
Created on Mon Jul 25 09:06:32 2016

@author: rayan
"""
import pandas as  pd
from sklearn import cross_validation
import pickle
import os
import csv
import sklearn

X = pd.read_csv("save/Xy_emb_part.csv").as_matrix()

X_train, X_test = cross_validation.train_test_split(X, train_size=0.8)

with open(  os.path.join("save", "preprocessed_set", "test_set.pkl"), "wb" ) as f:
    pickle.dump([X_test], f)

with open(  os.path.join("save", "preprocessed_set", "train_set.csv"), "w", newline='',encoding='UTF-8') as f:
    writer = csv.writer(f)
    writer.writerows(X_train)