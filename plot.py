# -*- coding: utf-8 -*-
"""
Created on Tue Jul 26 10:42:43 2016

@author: Rayan ELLEUCH
"""
import joblib
import matplotlib.pyplot as plt


frames = joblib.load("frames.pkl")

plt.plot(frames)