# -*- coding: utf-8 -*-
"""
Created on Tue Jul 19 10:06:27 2016

@author: Rayan ELLEUCH
"""

import tensorflow as tf
import csv
import numpy as np
from sklearn import preprocessing
import pandas as pd
import pickle
import os
from sklearn.metrics import confusion_matrix

def load():
    with open(os.path.join("save", "save.pkl"), "rb" ) as f:
        categories, phone, device, clf_pos, lb_pos, lb_categories, lb_phone, lb_device, lb_group, le_phone, le_device, le_group = pickle.load(f)

    return le_phone, le_device, le_group, lb_group
    
le_phone, le_device, le_group, lb_group = load()

chunksize = 200
for chunk in pd.read_csv(os.path.join("save", "preprocessed_set", "train_set.csv"), chunksize=chunksize):
    #print(chunk.as_matrix()) 
    X = chunk.as_matrix()
    break

dim_X = np.asarray(X).shape[1]-1
dim_group = le_group.classes_.shape[0]

######

with open(  os.path.join("save", "preprocessed_set", "test_set.pkl"), "rb" ) as f:
    X_test, = pickle.load(f)
    
phone_test = X_test[:,0]
device_test = X_test[:,1]
features_test = X_test[:,2:dim_X+1]
features_test = np.asarray([[0 if po <1 else 1 for po in ar] for ar in features_test])
labels_test = lb_group.transform(X_test[:,-1])

filename_queue = tf.train.string_input_producer([os.path.join("save", "preprocessed_set", "train_set.csv")])

reader = tf.TextLineReader()
key, value = reader.read(filename_queue)

record_defaults = [[1.0]]*(dim_X+1)
cols = tf.decode_csv(
    value, record_defaults=record_defaults)
features = tf.pack(cols[2:dim_X+1])
labels = cols[-1]
phones = cols[0]
devices = cols[1]

phones_batch, devices_batch, features_batch, labels_batch = tf.train.shuffle_batch(
    [phones, devices, features, labels], batch_size=500,
    capacity=3000,
    min_after_dequeue=2000)

#####
embedding_size = 10

vocabulary_size = len(le_phone.classes_)
embeddings_phone = tf.Variable(
    tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0))

vocabulary_size = len(le_device.classes_)
embeddings_device = tf.Variable(
    tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0))

#####

p = tf.placeholder(tf.int32, [None])
d = tf.placeholder(tf.int32, [None])
x = tf.placeholder(tf.float32, [None, dim_X-1])

embed_phone = tf.nn.embedding_lookup(embeddings_phone, p)
embed_device = tf.nn.embedding_lookup(embeddings_device, d)

hidder1_size = 4000
W1 = tf.Variable(tf.truncated_normal([dim_X-1 + 2*embedding_size, hidder1_size], stddev=0.1))
b1 = tf.Variable(tf.zeros([hidder1_size]))
#tf.constant(0.1, shape=shape)
#tf.zeros([hidder1_size])

#truc = tf.concat(1, [embed_phone, embed_device, x])

entry = tf.concat(1, [embed_phone, embed_device, x])
y1 = tf.nn.sigmoid(tf.matmul(entry, W1) + b1)
#y1 = tf.nn.sigmoid(tf.matmul(entry, W1) + b1)

W_out = tf.Variable(tf.truncated_normal([hidder1_size, dim_group], stddev=0.1))
b_out = tf.Variable(tf.zeros([dim_group]))
#b_out = tf.Variable(tf.constant(0.1, shape=[dim_group])) #tf.Variable(tf.zeros([dim_group]))

output = tf.matmul(y1, W_out) + b_out
y = tf.nn.softmax(output)

y_ = tf.placeholder(tf.float32, [None, dim_group])
cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))

train_step = tf.train.GradientDescentOptimizer(0.01).minimize(cross_entropy)
#train_step = tf.train.MomentumOptimizer(0.01, 0.9).minimize(cross_entropy)

prediction = tf.argmax(y, 1)
correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# train
with tf.Session() as s:
    s.run(tf.initialize_all_variables())
    
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    
    
    
    for i in range(10000):
        p_val, d_val, features_val, labels_val = s.run([phones_batch, devices_batch, features_batch, labels_batch])
        #print(p_val)
        features_val = np.asarray([[0 if po <1 else 1 for po in ar] for ar in features_val])
        
        labels_val = lb_group.transform(labels_val)
        if i % 100 == 0:
            train_accuracy = accuracy.eval(feed_dict={p: phone_test, d : device_test, x : features_test, y_: labels_test})
            print('step {0}, training accuracy {1}'.format(i, train_accuracy))
            print("cost", cross_entropy.eval(feed_dict={p: phone_test, d : device_test, x : features_test, y_: labels_test}))
            #print(y.eval(feed_dict={p: p_val, d : d_val, x : features_val})[0])
            
            y_now = prediction.eval(feed_dict={p: phone_test, d : device_test, x : features_test})
            cm = confusion_matrix(X_test[:,-1], y_now)
            print(cm)
            #b = y1.eval(feed_dict={p: p_val, d : d_val, x : features_val})
            #print("b",b[0])
            #b = y.eval(feed_dict={p: p_val, d : d_val, x : features_val})
            #print(b[0])
            #print("entry", entry.eval(feed_dict={p: p_val, d : d_val, x : features_val}))
            #print("w1",W1.eval())
            #train_accuracy = accuracy.eval(feed_dict={x: test_set, y_: label})
            #print('step {0}, training accuracy {1}'.format(i, train_accuracy))
            #print(y.eval(feed_dict={x: test_set})[0])
            
        s.run(train_step, feed_dict={p: p_val, d : d_val, x : features_val, y_: labels_val})
    
    coord.request_stop()
    coord.join(threads)