# -*- coding: utf-8 -*-
"""
Created on Thu Jul 28 09:23:15 2016

@author: Rayan ELLEUCH
"""

import pickle
import pandas as pd
import os

import sklearn
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import confusion_matrix
from sklearn.grid_search import GridSearchCV
from sklearn.svm import SVC

if __name__ == '__main__':
    val_train = pd.read_csv(os.path.join("save", "preprocessed_set", "train_set.csv")).as_matrix()
    
    with open(os.path.join("save", "preprocessed_set", "test_set.pkl"), "rb" ) as f:
        val_test, = pickle.load(f)
    
    lenght = val_test.shape[1]
    
    X_test = val_test[:,:lenght-1]
    y_test = val_test[:, -1]
    
    X_train = val_train[:,:lenght-1]
    y_train = val_train[:, -1]
    
    
    clf = RandomForestClassifier(n_estimators=100)
    
    clf.fit(X_train, y_train)
    
    cmtr = confusion_matrix(clf.predict(X_train), y_train)
    print(cmtr.diagonal().sum()/cmtr.sum())
    
    cmte = confusion_matrix(clf.predict(X_test), y_test)
    print(cmte.diagonal().sum()/cmte.sum())
    
    clf = ExtraTreesClassifier(n_estimators=15)
    
    clf.fit(X_train, y_train)
    
    cmtr = confusion_matrix(clf.predict(X_train), y_train)
    print(cmtr.diagonal().sum()/cmtr.sum())
    
    cmte = confusion_matrix(clf.predict(X_test), y_test)
    print(cmte.diagonal().sum()/cmte.sum())
    
    
    tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                         'C': [1, 10, 100, 1000]},
                        {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
     
    scores = ['precision', 'recall']
    for score in scores:
        print("# Tuning hyper-parameters for %s" % score)
        print()
        clf = GridSearchCV(SVC(C=1), tuned_parameters, cv=5, n_jobs=3,
                               scoring='%s_weighted' % score)
        
        clf.fit(X_train, y_train)
        print(clf.best_params_)