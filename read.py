import numpy as np
import pandas as pd
import os
import time
import matplotlib.pyplot as plt
#from mpl_toolkits.basemap import Basemap
import colorsys
from sklearn import preprocessing
from sklearn.cluster import KMeans
import csv
import pickle

with open( "save\Xy_emb_part.csv", "w", newline='',encoding='UTF-8') as f:
    pass

print("Load")
gender_age_train = pd.read_csv(os.path.join("data", "gender_age_train.csv"))

events = pd.read_csv(os.path.join("data", "events.csv"))

app_events = pd.read_csv(os.path.join("data", "app_events.csv"))

phone_brand_device_model = pd.read_csv(os.path.join("data", "phone_brand_device_model.csv"))

app_labels = pd.read_csv(os.path.join("data", "app_labels.csv"))

label_categories = pd.read_csv(os.path.join("data", "label_categories.csv"))

gender_age_test= pd.read_csv(os.path.join("data", "gender_age_test.csv"))


#####
#create cluster, label
print("create/load cluster, label")
if not os.path.isfile("save\save.pkl"):
    categories = label_categories["category"].unique()
    phone = phone_brand_device_model["phone_brand"].unique()
    device = phone_brand_device_model["device_model"].unique()
    group = gender_age_train["group"].unique()
    """
    le_categories = preprocessing.LabelEncoder()
    le_categories.fit(list(categories))

    """
    le_phone = preprocessing.LabelEncoder()
    le_phone.fit(list(phone))
    
    le_device = preprocessing.LabelEncoder()
    le_device.fit(list(device))
    
    le_group = preprocessing.LabelEncoder()
    le_group.fit(list(group))
    
    lat = events.latitude
    long = events.longitude
    entry = [[a, b] for a, b in zip(long, lat)]
    
    n = 20
    clf_pos = KMeans(n_clusters=n)
    clf_pos.fit(entry)
    
    lb_pos = preprocessing.LabelBinarizer()
    lb_pos.fit(clf_pos.labels_)
    
    lb_categories = preprocessing.LabelBinarizer()
    lb_categories.fit([str(x) for x in categories])
    
    lb_phone = preprocessing.LabelBinarizer()
    lb_phone.fit([str(x) for x in phone])
    
    lb_device = preprocessing.LabelBinarizer()
    lb_device.fit([str(x) for x in device])
    
    lb_group = preprocessing.LabelBinarizer()
    lb_group.fit(le_group.transform(list(group)))
    
    with open( "save\save.pkl", "wb" ) as f:
        pickle.dump( [categories, phone, device, clf_pos, lb_pos, lb_categories, lb_phone, lb_device, lb_group, le_phone, le_device, le_group], f )
else:
    print("  by importing")
    with open( "save\save.pkl", "rb" ) as f:
        categories, phone, device, clf_pos, lb_pos, lb_categories, lb_phone, lb_device, lb_group, le_phone, le_device, le_group = pickle.load(f)

print("Start")
####
X = []
#y = []

#with open("save\smthg.csv", 'w', newline='') as f:
#    writer = csv.writer(f)
for index, row in gender_age_train.iterrows():
    print(index, index/gender_age_train.shape[0])
    device_id = row["device_id"]
    gender = row["gender"]
    age = row["age"]
    group = row["group"]
    #print("numb event", id_events.size)
    
    id_phone = phone_brand_device_model.loc[phone_brand_device_model['device_id'] == device_id]
    phone = id_phone["phone_brand"].as_matrix()
    device = id_phone["device_model"].as_matrix()

    id_events = events.loc[events['device_id'] == device_id]
    
    id_pos_bin = np.zeros(len(lb_pos.classes_))
    id_categories_bin = np.zeros(len(lb_categories.classes_))
    if not id_events.empty:
        lat = id_events.latitude
        long = id_events.longitude
        entry = [[a, b] for a, b in zip(long, lat)]
        pos_label = clf_pos.predict(entry)
        
        temp = lb_pos.transform(pos_label)
        for p in temp:
            id_pos_bin += p
            
        save = app_events.loc[app_events['event_id'].isin(id_events['event_id'])]
        if not save.empty:
            apps = app_labels[app_labels['app_id'].isin(save['app_id'])]
            
            label = label_categories[label_categories['label_id'].isin(apps['label_id'])]
            id_categories = np.unique(label.category.as_matrix())
            
            temp = lb_categories.transform(id_categories)
            for p in temp:
                id_categories_bin += p

        
        """
        #print("searching")
        save = pd.DataFrame()
        chunksize = 100000
        start = time.time()
        for chunk in pd.read_csv(os.path.join("data", "app_events.csv"), chunksize=chunksize):
            a = chunk.loc[chunk['event_id'].isin(id_events['event_id'])]
            if not a.empty:
                save = save.append(a)
                #print("inter", save.size)
        #print("end", save.size)
        print(time.time()-start)
        apps = app_labels[app_labels['app_id'].isin(save['app_id'])]
        
        
        label = label_categories[label_categories['label_id'].isin(apps['label_id'])]
        #print(label.category.value_counts())
        """

    #y.append(group)
    features = []
    #features.extend([device_id])
    if len(le_phone.transform(phone)[0:]) > 1:
        print(le_phone.transform(phone)[0:], phone)
        break
    features.extend(le_phone.transform(phone)[0:1])
    features.extend(le_device.transform(device)[0:1])
    features.extend(list(id_pos_bin))
    features.extend(list(id_categories_bin))
    features.extend(le_group.transform([group])[0:])
    #print(features)
    X.append(features)
    
    if index >= 300:
        break
    #writer.writerow(features)

with open( "save\Xy_emb_part.csv", "w", newline='',encoding='UTF-8') as f:
    writer = csv.writer(f)
    writer.writerows(X)
"""
with open( "save\y_emb_part.csv", "w", newline='' ) as f:
    writer = csv.writer(f)
    writer.writerows([[i] for i in y])


from sklearn.mixture import GMM
clf = GMM(n_components=n, n_iter=10)

clf.fit(entry)
f = clf.predict(entry)
color = [colorsys.hls_to_rgb(i/n, 0.5, 0.5) for i in f]

m = Basemap(projection='cyl')
m.drawcoastlines()
m.drawcountries()


x, y = m(long, lat)

m.scatter(x, y,color=color)


plt.figure()


from sklearn.cluster import KMeans
clf = KMeans(n_clusters=n)

clf.fit(entry)
f = clf.predict(entry)
color = [colorsys.hls_to_rgb(i/n, 0.5, 0.5) for i in f]

m = Basemap(projection='cyl')
m.drawcoastlines()
m.drawcountries()


x, y = m(long, lat)

m.scatter(x, y,color=color)

plt.show()
"""